//
//  MainRouter.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 21/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

/// Роутер для главного экрана приложения
struct MainRouter {
    /// Базовая релизация роутера
    private let baseRouter: RouterMaker = BaseRouter()
    
    /// Переход к экрану с большим пальцем
    func toThumbs() {
        let controller = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: ThumbsViewController.storyboardIdentifier)
        baseRouter.show(controller)
    }
    
    /// Переход к экрану авторизации
    func toLogin() {
        let controller = UIStoryboard(name: "Login", bundle: nil)
            .instantiateViewController(withIdentifier: LoginViewController.storyboardIdentifier)
        baseRouter.setAsRoot(UINavigationController(rootViewController: controller))
    }
}

//
//  MainViewController.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 21/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

/// Главный экран приложения
class MainViewController: UIViewController {
    
    let router = MainRouter()

    @IBAction func toThumbs(_ sender: UIButton) {
        router.toThumbs()
    }
    
    @IBAction func toLogin(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        router.toLogin()
    }
    
}


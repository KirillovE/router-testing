//
//  LoginRouter.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 21/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

/// Роутер для экрана авторизации
struct LoginRouter {
    /// Базовая релизация роутера
    private let baseRouter: RouterMaker = BaseRouter()
    
    /// Переход к главному экрану приложения
    func toMain() {
        let controller = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: MainViewController.storyboardIdentifier)
        baseRouter.setAsRoot(UINavigationController(rootViewController: controller))
    }
    
    /// Переход к экрану восстановления пароля
    func toRecover() {
        let controller = UIStoryboard(name: "Login", bundle: nil)
            .instantiateViewController(withIdentifier: RecoverViewController.storyboardIdentifier)
        baseRouter.show(controller)
    }
}

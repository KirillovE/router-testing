//
//  RecoverViewController.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 21/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

/// Экран восстановления пароля
class RecoverViewController: UIViewController {
    
    @IBAction func recover(_ sender: UIButton) {
        let alert = UIAlertController(title: "Восстановление пароля",
                                      message: "Введите любой пароль или оставьте поле пустым",
                                      preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Хорошо", style: .cancel)
        alert.addAction(okButton)
        present(alert, animated: true)
    }
    
}

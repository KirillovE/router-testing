//
//  LoginViewController.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 21/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

/// Экран авторизации
class LoginViewController: UIViewController {
    
    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var password: UITextField!
    
    let router = LoginRouter()
    
    @IBAction func login(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isLoggedIn")
        router.toMain()
    }
    
    @IBAction func recover(_ sender: UIButton) {
        router.toRecover()
    }
    
}

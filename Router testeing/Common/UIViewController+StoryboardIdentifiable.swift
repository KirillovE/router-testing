//
//  UIViewController+StoryboardIdentifiable.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 21/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

/// Обязательное наличие идентификатора раскадровки (storyboard)
protocol StoryboardIdentifiable {
    /// Идентификатор в раскадровке
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

extension UIViewController: StoryboardIdentifiable {}

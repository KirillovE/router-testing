//
//  AppDelegate.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 21/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let controller: UIViewController
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            controller = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: MainViewController.storyboardIdentifier)
        } else {
            controller = UIStoryboard(name: "Login", bundle: nil)
                .instantiateViewController(withIdentifier: LoginViewController.storyboardIdentifier)
        }
        
        window = UIWindow()
        window?.rootViewController = UINavigationController(rootViewController: controller)
        window?.makeKeyAndVisible()
        
        return true
    }
    
}

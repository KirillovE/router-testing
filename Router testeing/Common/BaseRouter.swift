//
//  BaseRouter.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 22/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

/// Базовый роутер, соответствующий протоколу
struct BaseRouter: RouterMaker {
    var presenter: UIViewController? {
        return getTopController()
    }
    
    func show(_ controller: UIViewController) {
        presenter?.show(controller, sender: nil)
    }
    
    func presentModally(_ controller: UIViewController) {
        presenter?.present(controller, animated: true)
    }
    
    func setAsRoot(_ controller: UIViewController) {
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
}

// MARK: - Расширение для получения верхнего контроллера
extension BaseRouter {
    
    /// Возвращает верхний контроллер экрана
    ///
    /// - Returns: Верхний контроллер
    func getTopController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        } else {
            return nil
        }
    }
    
}

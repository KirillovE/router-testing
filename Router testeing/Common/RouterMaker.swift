//
//  RouterMaker.swift
//  Router testeing
//
//  Created by Евгений Кириллов on 21/01/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

/// Каркас для создания роутера
protocol RouterMaker {
    var presenter: UIViewController? { get }
    
    /// Показывает новый контроллер способом, определяемым текущим контроллером
    ///
    /// - Parameter controller: Контроллер для показа
    func show(_ controller: UIViewController)
    
    /// Показывает новый контроллер модально
    ///
    /// - Parameter controller: Контроллер для показа
    func presentModally(_ controller: UIViewController)
    
    /// Устанавливает в окно новый корневой контроллер
    ///
    /// - Parameter controller: Контроллер для показа
    func setAsRoot(_ controller: UIViewController)
}
